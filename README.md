# Google Hashcode 2017

[Homepage](https://hashcode.withgoogle.com)    
[Judge System](https://hashcodejudge.withgoogle.com/#/home)

## Team T.A.B. repository

* Brunetto Ziosi
* Luca Borsato
* Mario Spera

![alt text](https://gitlab.com/brunetto/ghc2017/raw/master/img/logo-mini.png  "Team TAB logo")

## Submissions

Main file (for easier submissions): [main.go](https://gitlab.com/brunetto/ghc2017/blob/master/cmd/ghc/main.go)

### Sub 1

### Sub 2

### Sub 3

### Sub 4

### Sub 5

### Sub 6

```go
localRating = float64(r.NumRequests) / float64(d.Videos[r.VideoID].Size)
			// Submission 0
			// niet
			//Submission 1
			//localRating /= endpoint.AvgLatency
			//Submission 2
			//localRating /= endpoint.Norm2Latency
			//Submission 3
			//localRating *= (float64(endpoint.DataCenterLatency) - endpoint.AvgLatency / endpoint.AvgLatency)//Submission 3
			//Submission 4
			//localRating *= float64(d.Videos[r.VideoID].Size) * (float64(endpoint.DataCenterLatency) - endpoint.AvgLatency / endpoint.AvgLatency)
			//Submission 5
			//localRating *= float64(d.Videos[r.VideoID].Size)
			//Submission 6
			//localRating *= float64(d.Videos[r.VideoID].Size) * (float64(endpoint.DataCenterLatency) - endpoint.AvgLatency )
			//Submission 7
			//localRating *= math.Sqrt(float64(d.Videos[r.VideoID].Size)) * (float64(endpoint.DataCenterLatency) - endpoint.AvgLatency )
			//Submission 8
			//localRating *= float64(d.Videos[r.VideoID].Size) * (float64(endpoint.DataCenterLatency) - endpoint.AvgLatency )
			//Submission 9
			localRating *= float64(d.Videos[r.VideoID].Size) * (float64(endpoint.DataCenterLatency) / endpoint.AvgLatency)
```

### Sub 7

**Score:** 1904022 

| Dataset | Points |
|:-------- | --------: |
| Kittens |569450|
|  Me at the zoo   | 477777  |
|Trending today|352532|
| Videos worth spreading  | 500411  |

**Notes:**
* May be some input datasets are commented

### Sub 8

**Score:** 1911644 

| Dataset | Points |
|:-------- | --------: |
| Kittens |570275|
|  Me at the zoo   | 484783  |
|Trending today|352428|
| Videos worth spreading  | 501027  |


* **Notes:**
    * Allow cache filling = cache max dimension
    * Uncomment input datasets
    * `Videos rating = endpointVideo.NumRequests * (endpoint.DataCenterLatency - endpoint.AvgLatency)`

### Sub 9

* **Score:**  1911644

| Dataset | Points |
|:-------- | --------: |
| Kittens |570275|
|  Me at the zoo   | 484783  |
|Trending today|352428|
| Videos worth spreading  | 501027  |

* **Notes:**
    * `Videos rating = endpointVideo.NumRequests * (endpoint.DataCenterLatency / endpoint.AvgLatency)`

### Sub 10

* **Score:**  1911644

| Dataset | Points |
|:-------- | --------: |
| Kittens |549639|
|  Me at the zoo   | 473370  |
|Trending today|352573|
| Videos worth spreading  | 489181  |

* **Notes:**
    * As sub9 with rating = num req for video | endpoint (localRating = float64(r.NumRequests))
    * Resort on real gain for each video | endpoint and recache

