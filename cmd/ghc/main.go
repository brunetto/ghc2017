package main

import (
	"bufio"
	"fmt"
	"github.com/brunetto/goutils/readfile"
	"log"
	"math"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"
)

func main() {
	fileNames := []string{"me_at_the_zoo.in", "kittens.in", "trending_today.in", "videos_worth_spreading.in"}
	for _, fileName := range fileNames {
		t0 := time.Now()
		fmt.Printf("Starting file %v\n", fileName)
		data := Data{FileName: fileName, InFilesPath: "../../datasets/in/", OutFilesPath: "../../datasets/"}
		data.Load()

		// Distribute videos on cache servers
		data.Caching()

		// Dump result
		str := data.SprintOutput()
		data.DumpResult(str)

		t1 := time.Now()
		fmt.Printf("Wall time for file %v: %v\n", fileName, t1.Sub(t0))
	}
}

type Data struct {
	Caches                        []Cache
	CacheSizeLimit                int
	Endpoints                     map[int]Endpoint
	InFilesPath                   string
	OutFilesPath                   string
	FileName                      string
	// Array with the video requests to be placed
	GlobalEndpointsVideosRequests EndpointsVideosRequests
	NumVideos                     int
	NumEndpoints                  int
	NumRequestsDescriptions       int
	NumCaches                     int
	Requests                      []Request
	Videos                        []Video
}

func (d *Data) Caching() {
	fmt.Println("Start caching")
	fmt.Println("Sort endpoints latencies")
	// Descending sort of videos requests
	for _, e := range d.Endpoints {
		sort.Sort(e.CacheLatencies)
	}

	fmt.Println("Retrieving video requests from all the endpoints")
	// Create global list of videos-endpoints-caches-requests-rating and sort it on rating
	globalRequests := EndpointsVideosRequests{}
	for _, e := range d.Endpoints {
		globalRequests = append(globalRequests, e.EndpointsVideosRequests...)
	}
	sort.Sort(globalRequests)
	d.GlobalEndpointsVideosRequests = globalRequests

	fmt.Println("Distributing videos")

	globalReserve := EndpointsVideosRequests{}
	kdx :=0
	for {
		fmt.Println("Distribute videos on cache servers based on previous sort")
		for _, evr := range d.GlobalEndpointsVideosRequests {
			fmt.Println("")
			// Avoid caching videos with zero or negative rating (too big or cache latency larger than data center latency)
			if evr.VideoLocalRating <= 0 {
				break
			}

			//fmt.Println("Try to add video to cache, if one fail, try the next")
			for _, cl := range d.Endpoints[evr.EndpointID].CacheLatencies {
				c := d.Caches[cl.ID]
				// Add video to cache
				evr.UpdatedRating = float64(evr.NumRequests) * float64(d.Endpoints[evr.EndpointID].DataCenterLatency - cl.Latency)
				added, reserve := c.AddVideoToCache(evr)


				// flush reserve for this cache on global reserve
				globalReserve = append(globalReserve, reserve...)
				if added {
					//fmt.Printf("%+v\n", c)
					// Save updated cache
					d.Caches[cl.ID] = c
					break
				}
				// Try next cache
			}
		}

		if len(d.GlobalEndpointsVideosRequests) == len(globalReserve){
			break
		}

		// Sort on VideoLocalRating = Number of requests for video given the endpoint
		sort.Sort(globalReserve)

		fmt.Printf("%v: %v\n\n\n", kdx, globalReserve)

		// Evaluate global reserve
		d.GlobalEndpointsVideosRequests = globalReserve
	}
}

type Cache struct {
	ID        int
	Filling   int
	//VideosIDs []int
	EndpointsVideosRequests EndpointsVideosRequests
	SizeLimit int
}

func (c *Cache) AddVideoToCache(videoRequest EndpointsVideoRequests) (bool, EndpointsVideosRequests)  {

	//fmt.Printf("%+v\n", videoRequest)

	reserve := EndpointsVideosRequests{}

	// If video already stored, say ok and go on
	for _, vreq := range c.EndpointsVideosRequests {
		if videoRequest.VideoId == vreq.VideoId {
			return true, EndpointsVideosRequests{}
		}
	}

	// Change type to sort on the right attribute
	updatedEndpointsVideosRequests := make(UpdatedEndpointsVideosRequests, len(c.EndpointsVideosRequests))
	for idx, vrq := range c.EndpointsVideosRequests{
		updatedEndpointsVideosRequests[idx] = vrq
	}

	// Can't store video, not enough space left
	if c.Filling+videoRequest.VideoSize > c.SizeLimit {

		// If rating is not enough, exit and knock to the next cache server
		if videoRequest.UpdatedRating < c.EndpointsVideosRequests[len(c.EndpointsVideosRequests)-1].UpdatedRating {
			return false, EndpointsVideosRequests{}
		}

		sort.Sort(updatedEndpointsVideosRequests)

		cumulativeRating := 0.
		cumulativeSize := 0

		// Check if the new video can take the place of a stored video
		for i:=len(updatedEndpointsVideosRequests)-1; i>=0; i-- {

			cumulativeRating += updatedEndpointsVideosRequests[i].UpdatedRating
			cumulativeSize += updatedEndpointsVideosRequests[i].VideoSize

			if videoRequest.UpdatedRating > cumulativeRating {
				continue
			}

			if videoRequest.UpdatedRating < cumulativeRating {
				// Save videos for next cache
				for j := i; j < len(updatedEndpointsVideosRequests); j++ {
					reserve = append(reserve, updatedEndpointsVideosRequests[j])
				}
				// Delete videos
				updatedEndpointsVideosRequests = updatedEndpointsVideosRequests[:i+1]

				// Add video
				updatedEndpointsVideosRequests = append(updatedEndpointsVideosRequests, videoRequest)

				// Update saved cache
				c.EndpointsVideosRequests = make(EndpointsVideosRequests, len(updatedEndpointsVideosRequests))
				for idx, vrq := range updatedEndpointsVideosRequests{
					c.EndpointsVideosRequests[idx] = vrq
				}
				// Update filling
				c.Filling = c.Filling - cumulativeSize + videoRequest.VideoSize

				return true, reserve
			}

			if videoRequest.UpdatedRating == cumulativeRating {
				if videoRequest.VideoSize >= cumulativeSize {
					return false,  EndpointsVideosRequests{}
				} else {
					// Save videos for next cache
					for j := i; j < len(updatedEndpointsVideosRequests); j++ {
						reserve = append(reserve, updatedEndpointsVideosRequests[j])
					}
					// Delete videos
					updatedEndpointsVideosRequests = updatedEndpointsVideosRequests[:i+1]

					// Add video
					updatedEndpointsVideosRequests = append(updatedEndpointsVideosRequests, videoRequest)

					// Update saved cache
					c.EndpointsVideosRequests = make(EndpointsVideosRequests, len(updatedEndpointsVideosRequests))
					for idx, vrq := range updatedEndpointsVideosRequests{
						c.EndpointsVideosRequests[idx] = vrq
					}
					// Update filling
					c.Filling = c.Filling - cumulativeSize + videoRequest.VideoSize

					return true, reserve
				}
			}
		}
	}

	// Add video
	updatedEndpointsVideosRequests = append(updatedEndpointsVideosRequests, videoRequest)

	//fmt.Printf("CAZZO1 %+v\n", updatedEndpointsVideosRequests)

	// Update saved cache
	c.EndpointsVideosRequests = make(EndpointsVideosRequests, len(updatedEndpointsVideosRequests))
	for idx, vrq := range updatedEndpointsVideosRequests{
		c.EndpointsVideosRequests[idx] = vrq
	}

	//fmt.Printf("CAZZO2 %+v\n", c.EndpointsVideosRequests)

	// Update filling
	c.Filling += videoRequest.VideoSize
	return true, EndpointsVideosRequests{}
}

type Video struct {
	ID     int
	Size   int
	Rating float64
}

type EndpointCacheLatency struct {
	ID      int
	Latency int
}

func (e EndpointCacheLatencies) Avg() (avg float64) {
	avg = 0.
	for _, l := range e {
		avg += float64(l.Latency)
	}
	avg = avg / float64(len(e))

	return avg
}

func (e EndpointCacheLatencies) Norm2() (avg float64) {
	avg = 0.
	for _, l := range e {
		avg += (float64(l.Latency) * float64(l.Latency))
	}
	avg = math.Sqrt(avg)
	avg = avg / float64(len(e))

	return avg
}

type EndpointCacheLatencies []EndpointCacheLatency

func (e EndpointCacheLatencies) Len() int           { return len(e) }
func (e EndpointCacheLatencies) Swap(i, j int)      { e[i], e[j] = e[j], e[i] }
func (e EndpointCacheLatencies) Less(i, j int) bool { return e[i].Latency < e[j].Latency }

type Endpoint struct {
	ID                      int
	DataCenterLatency       int
	CacheLatencies          EndpointCacheLatencies
	NumCaches               int
	EndpointsVideosRequests EndpointsVideosRequests
	//TotRequests             int
	CacheSizeLimit int
	AvgLatency     float64
	Norm2Latency   float64
}

type EndpointsVideosRequests []EndpointsVideoRequests
type UpdatedEndpointsVideosRequests []EndpointsVideoRequests

type EndpointsVideoRequests struct {
	EndpointID  int
	VideoId     int
	VideoSize   int
	NumRequests int
	//VideoGlobalRating float64
	VideoLocalRating float64
	VideoLocalCacheRatingMap map[int]float64
	CacheID int
	UpdatedRating float64
}

func (e EndpointsVideosRequests) Len() int      { return len(e) }
func (e EndpointsVideosRequests) Swap(i, j int) { e[i], e[j] = e[j], e[i] }
func (e EndpointsVideosRequests) Less(i, j int) bool {
	return e[i].VideoLocalRating > e[j].VideoLocalRating
}

func (e UpdatedEndpointsVideosRequests) Len() int      { return len(e) }
func (e UpdatedEndpointsVideosRequests) Swap(i, j int) { e[i], e[j] = e[j], e[i] }
func (e UpdatedEndpointsVideosRequests) Less(i, j int) bool {
	return e[i].UpdatedRating > e[j].UpdatedRating
}

type Request struct {
	VideoID     int
	EndpointID  int
	NumRequests int // Rating
}

func (d *Data) Load() error {
	var (
		lr readfile.LineReader
		eof bool
		endpoint         Endpoint
		endpoints        []Endpoint
		requests         []Request
		request          Request
		err              error
		file             *os.File
		tmp              []int
		tmp0, tmp1, tmp2 int
	)

	file, err = os.Open(filepath.Join(d.InFilesPath, d.FileName))
	if err != nil {
		log.Fatal("Error opening file: ", err.Error())
	}
	defer file.Close()
	reader := bufio.NewReader(file)
	lr = readfile.NewLineReader(reader)

	tmp, err, eof = lr.LineToIntSlice(" ")
	if err != nil {
		log.Fatal(err)
	}

	d.NumVideos = tmp[0]
	d.NumEndpoints = tmp[1]
	d.NumRequestsDescriptions = tmp[2]
	d.NumCaches = tmp[3]
	d.CacheSizeLimit = tmp[4]

	// Create caches
	caches := []Cache{}
	for i := 0; i < d.NumCaches; i++ {
		caches = append(caches, Cache{ID: i, Filling: 0, SizeLimit: d.CacheSizeLimit, EndpointsVideosRequests: EndpointsVideosRequests{}})
	}
	d.Caches = caches

	fmt.Println("Read second line")
	d.Videos = []Video{}

	tmp, err, eof = lr.LineToIntSlice(" ")
	if err != nil {
		log.Fatal(err)
	}
	videos := []Video{}
	for i, dim := range tmp {
		d := dim
		videos = append(videos, Video{ID: i, Size: d})
	}
	d.Videos = videos

	fmt.Println("Loading endpoints")
	endpoints = []Endpoint{}
	for i := 0; i < d.NumEndpoints; i++ {
		// From third row on
		endpoint = Endpoint{ID: i,
			CacheLatencies: []EndpointCacheLatency{},
			EndpointsVideosRequests: []EndpointsVideoRequests{},
			CacheSizeLimit: d.CacheSizeLimit}

		tmp, err, eof = lr.LineToIntSlice(" ")
		switch {
		case err != nil && !eof:
			log.Fatal(err)
		case err!= nil && eof:
			break
		}
		endpoint.DataCenterLatency = tmp[0]
		endpoint.NumCaches = tmp[1]

		// Read endpoints caches
		for j := 0; j < endpoint.NumCaches; j++ {
			tmp, err, eof = lr.LineToIntSlice(" ")
			switch {
			case err != nil && !eof:
				log.Fatal(err)
			case err!= nil && eof:
				break
			}
			tmp0 = tmp[0]
			tmp1 = tmp[1]

			// Add cache to endpoint
			endpoint.CacheLatencies = append(endpoint.CacheLatencies, EndpointCacheLatency{ID: tmp0, Latency: tmp1})
		}
		// Add endpoint to the list
		endpoints = append(endpoints, endpoint)
	}
	// Store endpoints into data (a map)
	d.Endpoints = map[int]Endpoint{}
	for _, endpoint := range endpoints {
		d.Endpoints[endpoint.ID] = endpoint
	}

	fmt.Println("Loading requests")
	requests = []Request{}
	for i := 0; i < d.NumRequestsDescriptions; i++ {
		request = Request{}
		tmp, err, eof = lr.LineToIntSlice(" ")
		switch {
		case err != nil && !eof:
			log.Fatal(err)
		case err!= nil && eof:
			break
		}
		tmp0 = tmp[0]
		tmp1 = tmp[1]
		tmp2 = tmp[2]

		request.VideoID = tmp0
		request.EndpointID = tmp1
		request.NumRequests = tmp2
		requests = append(requests, request)
	}
	d.Requests = requests

	fmt.Println("Evaluating request per endpoint")
	for _, r := range d.Requests {
		// Load request into its endpoint
		endpoint = d.Endpoints[r.EndpointID]
		localRating := 0.
		endpoint.AvgLatency = endpoint.CacheLatencies.Avg()
		endpoint.Norm2Latency = endpoint.CacheLatencies.Norm2()
		if d.Videos[r.VideoID].Size < d.CacheSizeLimit {
			localRating = float64(r.NumRequests)
		}

		localRatingMap := map[int]float64{}

		//for _, cache := range endpoint.CacheLatencies {
		//	localRatingMap[cache.ID] = float64(r.NumRequests) * float64(endpoint.DataCenterLatency - cache.Latency)
		//}

		vreq := EndpointsVideoRequests{
			EndpointID:       endpoint.ID,
			VideoId:          r.VideoID,
			VideoSize:        d.Videos[r.VideoID].Size,
			NumRequests:      r.NumRequests, /*VideoGlobalRating: d.Videos[r.VideoID].Rating,*/
			VideoLocalRating: localRating,
			VideoLocalCacheRatingMap: localRatingMap,
		}
		endpoint.EndpointsVideosRequests = append(endpoint.EndpointsVideosRequests, vreq)
		d.Endpoints[r.EndpointID] = endpoint
	}

	return nil
}

func (d *Data) DumpResult(str string) {
	baseName := strings.TrimSuffix(d.FileName, filepath.Ext(d.FileName))
	file, err := os.Create(filepath.Join(d.OutFilesPath, baseName+".out"))
	if err != nil {
		log.Fatal("Error opening file: ", err.Error())
	}
	defer file.Close()
	writer := bufio.NewWriter(file)
	defer writer.Flush()
	_, err = writer.WriteString(str)
	if err != nil {
		log.Fatal("Error writing file: ", err.Error())
	}
}

func (d *Data) SprintInput() string {
	s := ""
	s += fmt.Sprintf("%v %v %v %v %v\n", d.NumVideos, d.NumEndpoints, d.NumRequestsDescriptions, d.NumCaches, d.CacheSizeLimit)

	for _, v := range d.Videos {
		s += fmt.Sprintf("%v ", v.Size)
	}
	s += "\n"

	// Print endpoints
	for _, e := range d.Endpoints {
		s += fmt.Sprintf("%v %v\n", e.DataCenterLatency, len(e.CacheLatencies))
		for id, lat := range e.CacheLatencies {
			s += fmt.Sprintf("%v %v\n", id, lat)
		}
	}

	for _, r := range d.Requests {
		s += fmt.Sprintf("%v %v %v\n", r.VideoID, r.EndpointID, r.NumRequests)
	}
	return s
}

func (d *Data) SprintOutput() string {
	s := ""

	usedCaches := 0
	for _, c := range d.Caches {
		if c.Filling == 0 {
			continue
		}
		usedCaches++
		s += fmt.Sprintf("%v ", c.ID)
		for _, vreq := range c.EndpointsVideosRequests {
			s += fmt.Sprintf("%v ", vreq.VideoId )
		}
		s += "\n"
	}
	s = fmt.Sprintf("%v\n", usedCaches) + s

	return s
}
