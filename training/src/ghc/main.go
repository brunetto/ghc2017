package main

import (
	ghc "gitlab.com/brunetto/ghc2017/training/src"
	"fmt"
	"time"
)

func main () () {
	var (
		data ghc.Data
	)

	for _, filename := range []string{ "small.in", "example.in", "medium.in", "big.in"} {

		t0 := time.Now()

		data = ghc.Data{
			FilesPath: "../../training",
			FileName:  filename,
		}

		data.Load()
		data.ExplorePizza()
		data.DumpResult(data.Sprint())
		t1 := time.Now()
		fmt.Printf("Wall time for file %v: %v\n", filename,  t1.Sub(t0))

	}
}
