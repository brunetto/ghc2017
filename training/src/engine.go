package ghc2017

import (
	"bufio"
	"fmt"
	"github.com/brunetto/goutils/readfile"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

type Data struct {
	FilesPath      string
	FileName       string
	Rows           int
	Columns        int
	MinIngredients int
	MaxCells       int
	Pizza          []string
	Slices []Slice
}

type Slice struct {
	R0, C0, R1, C1 int
}

func (s Slice) Sprint() string {
	return fmt.Sprintf("%v %v %v %v", s.R0, s.C0, s.R1, s.C1)
}

func (d *Data) CheckIngredients(r0, c0, r1, c1 int) bool {
	ingredients := d.SliceIngredients(r0, c0, r1, c1)
	if ingredients["T"] >= d.MinIngredients && ingredients["M"] >= d.MinIngredients {
		return true
	}
	return false
}

func (d *Data) SliceIngredients (r0, c0, r1, c1 int)  map[string]int {
	ingredients := map[string]int{"T":0, "M":0}
	for i:= r0;i<= r1;i++{
		for j:= c0;j<= c1;j++{
			ingredients[d.Pizza[d.At(i, j)]] +=1
		}
	}
	return ingredients
}

func (d *Data) CheckDimension(r0, c0, r1, c1 int) bool {
	return d.SliceDimension(r0,  c0, r1,c1)<=d.MaxCells
}

func (d *Data) SliceDimension(r0, c0, r1, c1 int) int {
	return (r1-r0+1)*(c1-c0+1)
}

func (d *Data) ExplorePizza () {
	for r0:=0;r0<d.Rows;r0++ {
		for c0:=0;c0 <d.Columns; c0++ {
			if d.Eaten(r0, c0) {
				continue
			}
			//fmt.Printf("Exploring from %v %v\n", r0, c0)
			d.ExploreSlice0(r0, c0)
		}
	}
}

func (d *Data) EatSlice(r0, c0, r1, c1 int) {
	for i:=r0;i<=r1;i++ {
		for j:=c0;j <=c1; j++ {
			d.Pizza[d.At(i, j)] = "-"
		}
	}
}

func (d *Data) Eaten (i, j int) bool {
	return d.Pizza[d.At(i, j)] == "-"
}

// Exits ASAP a valid slice if found
func (d *Data) ExploreSlice0(r0, c0 int) {
	for r1:=r0;r1<d.Rows;r1++ {
		for c1:=c0;c1<d.Columns;c1++ {
			//fmt.Printf("\tExploring to %v %v\n", r1, c1)
			if d.Eaten(r1, c1){
				return
			}
			if d.CheckDimension(r0, c0, r1, c1) {
				if d.CheckIngredients(r0, c0, r1, c1) {
					//fmt.Printf("Slice dim for %v, %v, %v, %v: %v, %v || %v, %v \n", r0, c0, r1, c1, d.SliceDimension(r0, c0, r1, c1), d.CheckDimension(r0, c0,r1,  c1), d.SliceIngredients(r0, c0, r1, c1), d.CheckIngredients(r0, c0,r1,  c1))
					d.Slices = append(d.Slices, Slice{r0, c0, r1, c1})
					d.EatSlice(r0, c0, r1, c1)
					//fmt.Println("Slice: ", len(d.Slices)-1)
					//d.PrintInputData()
					return
				}
			} else {
				return
			}
		}
	}
}

// It always tries to reach the max slice dimension, if the slice is valid, it appends it, else it only returns
func (d *Data) ExploreSlice1(r0, c0 int) {
	slice := Slice{-1, -1, -1, -1}
	for r1:=r0;r1<d.Rows;r1++ {
		for c1:=c0;c1<d.Columns;c1++ {
			if d.Eaten(r1, c1){
				return
			}
			if d.CheckDimension(r0, c0, r1, c1) {
				if d.CheckIngredients(r0, c0, r1, c1) {
					slice = Slice{r0, c0, r1, c1}
				}
			} else {
				if slice.R0 != -1 {
					d.Slices = append(d.Slices, slice)
					d.EatSlice(r0, c0, r1, c1)
				}
				return
			}
		}
	}
}



func (d *Data) Load() error {
	file, err := os.Open(filepath.Join(d.FilesPath, d.FileName))
	if err != nil {
		log.Fatal("Error opening file: ", err.Error())
	}
	defer file.Close()
	reader := bufio.NewReader(file)

	line, err := readfile.Readln(reader)
	if err != nil {
		if err.Error() != "EOF" {
			log.Fatal("Error while reading csv file!!!")
		}
		log.Fatal(err)
	}
	tmp := strings.Split(line, " ")

	d.Rows, err = strconv.Atoi(tmp[0])
	d.Columns, err = strconv.Atoi(tmp[1])
	d.MinIngredients, err = strconv.Atoi(tmp[2])
	d.MaxCells, err = strconv.Atoi(tmp[3])
	if err != nil {
		log.Fatal(err)
	}

	d.Pizza = make([]string, d.Rows*d.Columns)
	for i := 0; i < d.Rows; i++ {
		line, err := readfile.Readln(reader)
		if err != nil {
			if err.Error() != "EOF" {
				log.Fatal("Error while reading csv file!!!")
			}
			break
		}

		for j := 0; j < d.Columns; j++ {
			d.Pizza[d.At(i, j)] = string(line[j])
		}
	}

	d.Slices = []Slice{}

	return nil
}

func (d *Data) PrintInputData() {
	fmt.Println(d.Rows, d.Columns, d.MinIngredients, d.MaxCells)
	for i := 0; i < d.Rows; i++ {
		for j := 0; j < d.Columns; j++ {
			fmt.Print(d.Pizza[d.At(i, j)])
		}
		fmt.Println()
	}
}

func (d *Data) At(i, j int) int {
	return i*d.Columns + j
}

func (d *Data) Sprint()string{
	var s string
	s += strconv.Itoa(len(d.Slices)) + "\n"
	for _, sl := range d.Slices {
		s += sl.Sprint() + "\n"
	}
	return s
}

func (d *Data) DumpResult (str string) {
	baseName := strings.TrimSuffix(d.FileName, filepath.Ext(d.FileName))
	file, err := os.Create(filepath.Join(d.FilesPath, baseName + ".out"))
	if err != nil {
		log.Fatal("Error opening file: ", err.Error())
	}
	defer file.Close()
	writer := bufio.NewWriter(file)
	defer writer.Flush()
	_, err = writer.WriteString(str)
	if err != nil {
		log.Fatal("Error writing file: ", err.Error())
	}
}
