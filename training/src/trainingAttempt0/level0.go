package ghc2017

import "fmt"

func (out *Output) Level0 (in Input) {

	slice := NewSlice(0,0)

	for i:=0; i<in.Rows; i++ {
		for j:=0;j<in.Columns; j++ {
			c := in.Data[in.At(i, j)]
			slice.Ingredients[c]++
			switch  {
			case slice.Ingredients["T"] + slice.Ingredients["C"] > in.MaxCells:
				slice = NewSlice(i, j)
			case slice.Ingredients["T"] >= in.MinIngredients && slice.Ingredients["C"] >= in.MinIngredients:
				fmt.Println("Slices: ", len(out.Slices))
				slice.Close(i, j)
				out.Slices = append(out.Slices, slice)
				slice = NewSlice(i, j)
			default:
				// Do nothing
			}
		}
	}
	out.NSlices = len(out.Slices)
}

/*
* WRONG
* Walks along the line but doesn't reconsider the rectangle when starting a new line => slices are not rectangles
* Doesn't consider already cutted slices
*/
