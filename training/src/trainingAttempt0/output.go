package ghc2017

import (
	"fmt"
	"strconv"
)

type Slice struct {
	R1, R2, C1, C2 int
	Ingredients Ingredients
}

type Ingredients map[string]int

func NewSlice(r1, c1 int) Slice {
	return Slice {R1: r1, R2: r1, C1: c1, C2:c1, Ingredients:Ingredients{"T":0, "M":0}}
}

func (s *Slice) Eat() {

}

func (s *Slice) IsComplete(r2, c2, min int) bool {
	for i:=s.R1;i<=r2;i++ {
		for j:=s.C1;j<=c2;j++ {

		}
	}
	return s.Ingredients["T"] >= min && s.Ingredients["C"] >= min
}

func (s *Slice) IsValid(i, j, max int) {
	return
}

func (s *Slice) Close(r2, c2 int) {
	s.R2 = r2
	s.C2 = c2
}

type Output struct {
	NSlices int
	Slices []Slice
}

func (s Slice) Sprint() string {
	return fmt.Sprintf("%v %v %v %v", s.R1, s.C1, s.R2, s.C2)
}

func (out *Output) Sprint() string {
	var s string
	s += strconv.Itoa(out.NSlices) + "\n"
	for _, sl := range out.Slices {
		s += sl.Sprint() + "\n"
	}
	return s
}

func NewOutput() Output{
	return Output{Slices:[]Slice{}}
}